function apenasNumeros(string) {
    var numsStr = string.replace(/[^0-9]/g,'');
    return parseInt(numsStr);
}

// Altera destaque
document.querySelectorAll('.video-card').forEach(item => {
  item.addEventListener('click', event => {
  var highlight           = document.querySelector('.highlight')
  var thumbnail           = highlight.querySelector('.highlightImage')
  var subheadedr          = highlight.querySelector('.text h3')
  var title               = highlight.querySelector('.text h2')

  let newThumbnail        = item.getAttribute("data-thumbnail")
  let newTitle            = item.querySelector(".text h2").innerHTML
  let newSubheader        = item.querySelector(".text h3").innerHTML

  thumbnail.setAttribute('src', newThumbnail)
  title.innerHTML         = newTitle
  subheadedr.innerHTML    = newSubheader
  })
})

// Altera destaque
document.querySelectorAll('.colunista-card').forEach(item => {
  item.addEventListener('mouseenter', event => {
    if(item.parentElement.id.substr(14) > 04){
      var px_transform_novo = apenasNumeros(item.parentElement.parentElement.style.transform) + 466
      item.parentElement.parentElement.style.transform = (`translateX(-${px_transform_novo}px)`)
    }
  })

  item.addEventListener('mouseleave', event => {
    if(item.parentElement.id.substr(14) > 04){
     var px_transform_base = apenasNumeros(item.parentElement.parentElement.style.transform) - 466
     item.parentElement.parentElement.style.transform = (`translateX(-${px_transform_base}px)`)
    }
  })
})

// Gera loop carrossel detaque usando Splide
for (var x of document.querySelectorAll('.splide.videosCarrossel')) {
  var splide = new Splide(x, {
    autoWidth: true,
    direction: 'ttb',
    rewind: true,
    height: '637px',
    gap: 24,
    breakpoints: {
      850: {
        direction: 'LTR',
      },
    }
  })
  splide.mount()
  x.style.display = 'inherit'
}

// Gera loop carrossel colunistas usando Splide
for (var y of document.querySelectorAll('.splide.colunistas')) {
  var splide = new Splide(y, {
    autoWidth: true,
    rewind: true,
    gap: 24
  })
  splide.mount()
  y.style.display = 'flex'
}